// Data Types
let num = 100; // number
let str = "hello"; // string
let arr = [num, str]; // array
let obj = { one: 1, two: 2, arr: arr }; // object
let bool = true; // boolean

//array mapping
let newArr = [];
for (let i = 1; i <= 100; i++) {
    newArr.push(i);
}

mappedArr = newArr.map((j) => j * 2);
console.log(mappedArr);

// loops -> For loop
for (let i = 1; i <= 10; i++) {
    console.log(i);
}

//While Loop
let a = 100;
while (a >= 0) {
    console.log(a);
    a--;
}

// Do While Loop
let i = 0;
do {
    i += 1;
    console.log(i);
} while (i < 5);

// For of Loop
let arrayOfNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
for (let x of arrayOfNumbers) {
    console.log(x);
}

//For in loop
let objectOfStuff = { hello: "world", john: "doe" };
for (let x in objectOfStuff) {
    console.log(objectOfStuff[x]);
}

//Looping over a string
let naam = "javascript";
for (let x of naam) {
    console.log(x);
}

// Functions -> Regular functions
function sum(a, b) {
    return a + b;
}

// Anonymous functions
let sumAnonymousFunction = function (a, b) {
    return a + b;
};

//Arrow functions
let sumArrowFunction = (a, b) => a + b;

//Hoisting
// Hoisting is a phenomenon in javascript where variables and functions are available before they are defined, they just have to be defined in the same lexical scope.
// While functions as well as the function body is hoisted, only variable declarations are hoisted in javascript which means all variables declared with the var keyword are available before assignment but their values are set to null.

//Mathematical operations -> Addition
console.log(1 + 2);

//Subtraction
console.log(2 - 1);

//Multiplication
console.log(1 * 6);

//Division
console.log(6 / 2);

//Exponentiation
console.log(2 ** 6);

//Modulus operator -> Returns the remainder after division.
console.log(6 % 4); //Returns 2

//Increment operator
let numToIncrement = 1;
numToIncrement++;
console.log(numToIncrement);

//Decrement Operator
let numToDecrease = 5;
numToDecrease--;
console.log(numToDecrease);

//Variables
var numb = 1;
let anotherNum = 2;
const yetAnotherNum = 3;

// Promises
const dog = new Promise((res, rej) => {
    const rand = Math.random();
    if (rand < 0.5) {
        res();
    } else {
        rej();
    }
});

dog.then(() => {
    console.log("Yay we got a dog");
}).catch((err) => {
    console.log("No Dog ", err);
});

//Filter
let arrToBeFiltered = [1, 23, 45, 32, 45, 654, 32, 34];
let filteredArr = arrToBeFiltered.filter((i) => i > 100);
console.log(filteredArr);

//Object mapping
let objToBeMapped = { a: "hello", b: "world", c: "john", d: "doe" };
let mappedObj = Object.keys(objToBeMapped).map((a) => objToBeMapped[a] + " ");
console.log(mappedObj);

//Operation in object
var person = {
    firstName: "John",
    lastName: "Doe",
    age: 50,
    eyeColor: "blue",
    fullName: function () {
        return this.firstName + " " + this.lastName;
    },
};
console.log(person.firstName);
console.log(person.lastName);
console.log(person.age);
console.log(person.eyeColor);

console.log(person["firstName"]);
console.log(person["lastName"]);
console.log(person["age"]);
console.log(person["eyeColor"]);

console.log(person.fullName());
